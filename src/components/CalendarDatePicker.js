import React from 'react';
import moment from 'moment';
import 'moment/locale/vi';
import 'moment/locale/th';
import isTouchDevice from 'is-touch-device';
import 'react-dates/initialize';
import { DayPickerSingleDateController, DayPickerRangeController } from 'react-dates';
import { DayPickerRangeControllerExtendtion } from './DayPickerRangeControllerExtendtion'
import 'react-dates/lib/css/_datepicker.css';
// import './index.css';

const START_DATE = 'startDate';
const END_DATE = 'endDate';

class CalendarDatePicker extends React.Component {
  
  // startDate = moment(new Date())
  // endDate = moment(new Date())
  // focusedInput = null
  
  constructor(props) {
    super(props);
    this.state = {
      date: null,
      startDate: null, //moment(new Date()),
      endDate: null, //moment(new Date()),
      focusedInput: START_DATE
    };
    this.isTouchDevice = isTouchDevice()
  }

  render() {
    let localeProp = this.props.locale
    let rangeSelection = this.props.rangeSelection
    // let locale = rangeSelection
    //   ? localeProp == 'vi'
    //     ? vnRange
    //     : localeProp == 'th'
    //     ? thRange
    //     : enRange
    //   : localeProp == 'vi'
    //     ? vnSingle
    //     : localeProp == 'th'
    //     ? thSingle
    //     : enSingle
    
    moment.locale(localeProp == 'vi-VN' ? 'vi' : localeProp == 'th-TH' ? 'th' : 'en')

    let { date, startDate, endDate, focusedInput } = this.state
    let startDateString = startDate && startDate.format('YYYY-MM-DD');
    let endDateString = endDate && endDate.format('YYYY-MM-DD');
    // debugger
    return <div>
      {rangeSelection
      ? <DayPickerRangeController
        numberOfMonths={2}
        isOutsideRange={isOutsideRange}
        initialVisibleMonth={() => moment().add(-1, 'months')}
        startDate={startDate ? moment(startDate) : null} // momentPropTypes.momentObj or null,
        endDate={endDate ? moment(endDate) : null} // momentPropTypes.momentObj or null,
        onDatesChange={this.onDatesChange.bind(this)} // PropTypes.func.isRequired,
        focusedInput={focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
        onFocusChange={this.onFocusChange.bind(this)} // PropTypes.func.isRequired,
        renderMonthElement={null}
      />
      : <DayPickerSingleDateController
        date={date ? moment(date) : null} // momentPropTypes.momentObj or null,
        onDateChange={this.onDateChange.bind(this)} // PropTypes.func.isRequired,
        focused={true} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
        // onFocusChange={console.log} // PropTypes.func.isRequired,
      />}
      {/* <span onClick={console.log} className='btn btn-default'>Ken xồ</span>
      <span onClick={() => console.log(this.startDate + ' - ' + this.endDate)} className='btn btn-primary'>Auker</span> */}
      <div>{focusedInput + ' >> Từ ' + startDateString + ' đến ' + endDateString}</div>
      <div>Single date selected: {date && date.format("DD/MM/YYYY")}</div>
      <div>moment.locale('{localeProp}') = {moment.locale()}</div>
    </div> 
  }

  onDatesChange (value) {
    
    this.setState({
      startDate: value.startDate,
      endDate: value.endDate
    })
  }

  onDateChange (value) {
    console.log(value && value.format("DD/MM/YYYY"))
    this.setState({ date: value })
  }

  onFocusChange (input) {
    this.setState({ focusedInput: !input ? START_DATE : input })
  }

}

export default CalendarDatePicker;

const isOutsideRange = (day) => {
  if (!moment.isMoment(day))
    return true

  let currentDay = moment()

  return day.isAfter(currentDay, 'day')

}

const renderMonthElement = ({month, onMonthSelect, onYearSelect}) => {
  let years = []
  let currentYear = moment().year()
  for(let y = 10; y >= 0; y--)  //for 10 years
    years.push(currentYear - y)

  return <div style={{ display: 'flex', justifyContent: 'center' }}>
    <div>
      <select
        value={month.month()}
        onChange={(e) => { onMonthSelect(month, e.target.value); }}
      >
        {moment.months().map((label, value) => (
          <option value={value}>{label}</option>
        ))}
      </select>
    </div>
    <div>
      <select
        value={month.year()}
        onChange={(e) => {onYearSelect(month, e.target.value); }}
      >
        {years.map(y => <option value={y}>{y}</option>)}
        {/* <option value={moment().year() - 1}>Last year</option>
        <option value={moment().year()}>{moment().year()}</option>
        <option value={moment().year() + 1}>Next year</option> */}
      </select>
    </div>
  </div>
}
